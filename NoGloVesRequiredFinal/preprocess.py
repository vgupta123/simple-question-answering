##################################################
# Code for data processing of QA System
# Author : Maitrey Mehta
##################################################
import re
################################################################################################
# Reads the .story file and returns a dictonary with appropriate keys.
# Input : Filename (e.g- "/path/to/file/<filename>.story")
# Output: Python dictionary of type {"headline":headline, "date":date, "id":story_id,"text":text}
################################################################################################
def readStory(filename):
	data = open(filename).read()
	text = data.split("TEXT:")[1]
	meta = data.split("TEXT:")[0]
	story_id = meta.split("STORYID: ")[1]
	story_id = story_id.split("\n")[0]
	headline = meta.split("HEADLINE: ")[1].split("DATE: ")[0].split("\n")[0]
	date = meta.split("DATE: ")[1].split("STORYID:")[0].split("\n")[0]
	story_data = {"headline":headline, "date":date, "id":story_id,"text":text}
	return story_data


################################################################################
# Function that makes certain character level manipulation.
# Input: Text(string)
# Output: Text(string)
###############################################################################
def preprocessText(text):
	text = text.replace("."," . ")
	text = text.replace("Mr . ","Mr.")
	text = text.replace("Ms . ","Ms.")
	text = text.replace("Mrs . ","Mrs.")
	text = text.replace("Dr . ","Dr.")
	text = text.replace("No . ","No.")
	text = text.replace("Mt . ","Mt.")
	text = text.replace("St . ","St.")
	text = text.replace("U . S . A . ","U.S.A.")

	text = text.replace("U . S . ","U.S.")
	text = text.replace("B . C . ","B.C.")
	text = text.replace("D . C . ","D.C.")
	
	text = text.replace("\n\n","")
	text = text.replace("\n"," ")
	decmark_reg = re.compile('(?<=\d) . (?=\d)')
	text = decmark_reg.sub('.',text)
	return text


###################################################################################
# Function that processes the .questions file into an array of python dictionaries
# Input: The storyid of the story considered(string), directory(string)
# Output: Python List of dictionaries 
#		(eg- [{"qid": qid1, "question": question1, "diff": diff1},
#			  {"qid": qid2, "question": question2, "diff": diff2}
#				...])
##################################################################################
def getQuestions(storyid,directory):
	qfile = directory + storyid + ".questions"
	data = open(qfile).read()
	# We assume that all the entities are separable by \n as we figured
	# seeing most of the files
	data = data.split("\n")	
	questions=[]
	for i in xrange(0,len(data),4):
		if data[i]=="":
			break
		qid = data[i].split("QuestionID: ")[1]
		question = data[i+1].split("Question: ")[1]
		diff = data[i+2].split("Difficulty: ")[1]
		temp_dict={"qid": qid, "question": question, "diff": diff}
		questions.append(temp_dict)

	return questions

def getAnswers(storyid, directory):
	afile = directory + storyid + ".answers"
	data = open(afile).read()
	# We assume that all the entities are separable by \n as we figured
	# seeing most of the files
	data = data.split("\n")	
	answers={}
	for i in xrange(0,len(data),5):
		if data[i]=="":
			break
		qid = data[i].split("QuestionID: ")[1]
		answer = data[i+2].split("Answer: ")[1]
		answers[qid]=answer

	return answers