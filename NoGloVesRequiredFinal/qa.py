import os
import sys, json
from preprocess import readStory,preprocessText,getQuestions
#from stanfordcorenlp import StanfordCoreNLP
from sentencescoring import sentScore
from coref import resolve, print_resolved
#from pycorenlp import StanfordCoreNLP


#nlp = StanfordCoreNLP('http://localhost:9000')

#nlp = StanfordCoreNLP("/usr/local/lib/python2.7/dist-packages/stanfordcorenlp")

##################################################################
# Function that does NER tagging for each sentence in reading text
# Input: text (string)
# Output: List of list. Inner list contains tuples of ('word',NER tag)
# Function Author: Maitrey Mehta
###################################################################
def nerTagging(text):
	sentences = text.split(" . ")
	ner_tagged_sentences = []
	for sent in sentences:
		if sent == "" or sent=="":
			continue
		temp_arr = nlp.ner(sent)
		ner_tagged_sentences.append(temp_arr)
	return ner_tagged_sentences



if __name__=="__main__":
	input_file = sys.argv[1]
	dirandids = open(input_file).read().split("\n")
	directory = dirandids[0]

	if dirandids[-1] == "":
		story_ids = dirandids[1:-1]
	else:
		story_ids = dirandids[1:]
	for i in story_ids:
		story_file = directory+i+".story"
		story = readStory(story_file)
		story["text"] = preprocessText(story["text"])
		questions = getQuestions(story["id"],directory)
		
		#############NER Tagger called##################
		#ner_tagged_sentences = nerTagging(story["text"])
		#print ner_tagged_sentences
		#print nlp.ner()
		#################################################

		#################### Coref Resolution ###################################
		# output = nlp.annotate(story["text"], properties= {'annotators':'dcoref','outputFormat':'json','ner.useSUTime':'false'})
		# resolve(output)
		# story["text"] = print_resolved(output)
		##############################################
		
		sentScore(story["text"],questions,i)