import subprocess

input_file = "testinputfile.txt"
dirandids = open(input_file).read().split("\n")
directory = dirandids[0]

if dirandids[-1] == "":
	story_ids = dirandids[1:-1]
else:
	story_ids = dirandids[1:]
f = open("accuracies.txt","w+")
f.close()
f = open("accuracies.txt","a+")
f2 = open("precision.txt","w+")
f2.close()
f2 = open("precision.txt","a+")
f3 = open("recall.txt","w+")
f3.close()
f3 = open("recall.txt","a+")


for i in story_ids:
	result = subprocess.run(["perl","score-answers.pl","answers/"+i,"testset1/"+i+".answers"],stdout=subprocess.PIPE)
	res = str(result.stdout).split("=")[-1]
	#print(result.stdout)
	prec = str(result.stdout).split("=")[-2]
	rec = str(result.stdout).split("=")[-3]
	res = res.split("*")[0][1:]
	f2.write(str(round(float(prec[1:7])*100,2))+"\n")
	f3.write(str(round(float(rec[4:11])*100,2))+"\n")
	f.write(str(round(float(res[:6])*100,2))+"\n")

