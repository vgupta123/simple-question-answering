RESOURCES:
1. The Syntactic Similarity measure has been inspired from the lexical similarity measure shown in paper by Islam A., Inkpen D., "Semantic Text Similarity Using Corpus-Based Word Similarity and String Similarity".  Link: http://www.site.uottawa.ca/~diana/publications/tkdd.pdf. The logic is implemented by us. The code is available here(Self-Citation): https://github.com/caffeine96/TextSemanticSimilarity/blob/master/SyntacticSimilarity.py
2. The retirieval scoring system is partially inspired by  Cao J., Song B., "Talking Geckos". The logic is implemented by us. Link: https://www.slideshare.net/jiessiecao/talking-geckos-question-and-answering
3. The extraction rules have been formulated by the lecture slides. The logic is implemented by us.
4. StanfordCoreNLP. A NLP library used for coreference resolution. Link: https://stanfordnlp.github.io/CoreNLP/
5. Spacy. A NLP library used for NER. Link: https://spacy.io/usage/linguistic-features
6. NLTK, NLP library used for stemming, POS tagging and stop word removal. Link: https://www.nltk.org/

TIME ESTIMATE:
The code takes about 30 seconds on average to evaluate a document.

INDIVIDUAL CONTRIBUTIONS:
We had broadly divided work into two parts- 1) IR System and 2)Information Extraction System. The breakdown of tasks is as follows-
Maitrey Mehta: IR System that extracts best matched sentences(Token match scoring, TF/IDF), Coreference Resolution, Syntactic Similarity, Pipelining, Peer System
Vivek Gupta: Question Typing, Information Extraction given the best sentence from the IR system, Pipelining

ECCENTRICITIES:
1. The syntactic similarity code can be time consuming for unusually long words (Words greater than 20 characters). However, such word occurence is rare and hence the algorithm suits well as it makes the overall system robust to spelling mistakes.
2. The system is quite robust even if the input doesn't conform to the format specified in the document. But we encourage the usage of the specified format.
3. The program for Coreference Resolution has been implemented but not used in the current system since it deteriorated the results. We are in the process of fixing this.
4. The trace file that has the system output is output.trace
