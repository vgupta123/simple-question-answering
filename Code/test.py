import subprocess

input_file = "inputfile.txt"
dirandids = open(input_file).read().split("\n")
directory = dirandids[0]

if dirandids[-1] == "":
	story_ids = dirandids[1:-1]
else:
	story_ids = dirandids[1:]
f = open("accuracies.txt","w+")
f.close()
f = open("accuracies.txt","a+")

for i in story_ids:
	result = subprocess.run(["perl","score-answers.pl","answers/"+i,"developset/"+i+".answers"],stdout=subprocess.PIPE)
	res = str(result.stdout).split("=")[-1]
	res = res.split("*")[0][1:]
	f.write(str(round(float(res[:6])*100,2))+"\n")

