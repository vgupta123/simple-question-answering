################################################################		
# Code for calculating sentence scores for questions 
# Author : Vivek Gupta
################################################################
import sys
import nltk
import numpy as np
from nltk.tag.stanford import StanfordNERTagger
from stanfordcorenlp import StanfordCoreNLP
from infoExtraction import questionTyping

nlp = StanfordCoreNLP("/usr/local/lib/python2.7/dist-packages/stanford-corenlp-full-2018-10-05")

score_dict = {"weak_clue":1, "clue": 2, "good_clue":3, "confident":6, "slam_dunk": 20}
default_dict= {"weak_clue":0, "clue": 0, "good_clue":0, "confident":0, "slam_dunk": 0, "total_score": 0}

#################################################################
# Function that takes question and a sentence and computes 
# word match and reqards ner tags of question ner, answer ner, and question_type 
# Input : two strings question and sentence
# Output: List of dictionaries containing best score for sentence
#		  with the question
################################################################
def ScoreQuesAnswer(question, sentence):
	global score_dict
	question_dic = questionTyping(question)
	type_ques = question_dic["type"]
	ner_tagged = get_continous_tags(nlp.ner(sentence))
	pos_tagged  = nlp.pos_tag(sentence)
	current_dict = default_dict.copy()
	question_nertags = question_dic['ner_tagged']

	if type_ques == "Who":
		for item in ner_tagged:
			tag = item[1]
			if tag == "ORGANIZATION" or tag == "TITLE":
				current_dict["good_clue"] += score_dict["good_clue"]
				current_dict["good_clue"] += score_dict["good_clue"]
			if tag == "PERSON" or tag == "HUMAN":
				current_dict["confident"] += score_dict["confident"]
				current_dict["total_score"] += score_dict["good_clue"]
	if type_ques == "Where":
		LocationPrepList = ["in","at", "near", "inside"]
		for locationPrep in LocationPrepList:
			if locationPrep in sentence:
				current_dict["good_clue"] += score_dict["good_clue"]
				current_dict["total_score"] += score_dict["good_clue"]
		for item in ner_tagged:
			tag = item[1]
			if tag == "LOCATION":
				current_dict["confident"] += score_dict["confident"]
				current_dict["total_score"] += score_dict["confident"]
	if type_ques == "When":
		for item in ner_tagged:
			tag = item[1]
			if tag == "DATE" or tag =="TIME" or tag == "DURATION":
				current_dict["good_clue"] += score_dict["good_clue"]
				current_dict["total_score"] += score_dict["good_clue"]
			question_tags = ["last","first","order","start","happen"]
			sentence_tags = ["last","first","order","start","year"]
			for qtag in question_tags:
				for stag in sentence_tags:
					if (qtag in question) and (stag in sentence):
						current_dict["slam_dunk"] += score_dict["slam_dunk"]
						current_dict["total_score"] += score_dict["slam_dunk"]
	if type_ques == "How":
		if "much" in question_dic["question"]:
			for item in ner_tagged:
				tag = item[1]
				if tag == 'MONEY':
					current_dict["good_clue"] += score_dict["good_clue"]
					current_dict["total_score"] += score_dict["good_clue"]
		if "many" in question_dic["question"]:
			for item in ner_tagged:
				tag = item[1]
				if tag == "NUMBER":
					current_dict["good_clue"] += score_dict["good_clue"]
					current_dict["total_score"] += score_dict["good_clue"]
	if type_ques == "Which":
		if "city" in question_dic["question"]:
			for item in ner_tagged:
				tag = item[1]
				if tag == "CITY" or tag == "LOCATION":
					current_dict["good_clue"] += score_dict["good_clue"]
					current_dict["total_score"] += score_dict["good_clue"]
	if type_ques == "Which":
		if "city" in question_dic["question"]:
			for item in ner_tagged:
				tag = item[1]
				if tag == "CITY" or tag == "LOCATION":
					current_dict["good_clue"] += score_dict["good_clue"]
					current_dict["total_score"] += score_dict["good_clue"]

	## ToDo: Who Rules Scoring## 
	## Using Question NER Tags ##
	return current_dict


#################################################################
# Function that computes sentence similarity score of questions
# with sentences in text 
# Input : text in story(string), Array of question strings
################################################################
def sentScore(text,questions):
	sentences = text.split(" . ")
	for ques in questions:
		best_answer_score = 0
		for sent in sentences:
			if sent == " " or sent=="":
				continue
			current_dict = ScoreQuesAnswer(ques["question"],sent)
			if current_dict["total_score"] > best_answer_score:
				best_answer_score = current_dict["total_score"]
				best_sent = sent
				best_sent_score_dic = current_dict

		print "Question: " + str(ques["question"])
		print "Best Sentence: " + str(best_sent)
		print "Best Sent Scores: \n" + str(best_sent_score_dic)
		print "\n\n"
	nlp.close()