################################################################		
# Code for calculating lexical(syntactic) similarity of two words 
# Author : Maitrey Mehta
################################################################

###############################################################
# Function that calculates Longest Common Subsequence(LCS)
# ie. w1 = abcde and w2 = abd LCS = abd
# Input: X and Y are words and m and n are respective lengths 
# Output: Length of LCS
##############################################################	
def lcs(X, Y, m, n):
	if m == 0 or n == 0:
		return 0;
	elif X[m-1] == Y[n-1]:
		return 1 + lcs(X, Y, m-1, n-1);
	else:
		return max(lcs(X, Y, m, n-1), lcs(X, Y, m-1, n));



###################################################################
# Function that calculates Consequtive Longest Common Subsequence
# starting from the first character(CLCS1)
# ie. w1 = abcdef and w2 = abdef CLCS1 = ab
# Input: a and b are words 
# Output: Length of CLCS1
###############################################################
def clcs1(a,b):
	j=0;
	if len(b)<len(a):
		a,b = b,a

	ans=0;
	for i in range(0,len(a)):
		
		if a[i] == b[i] :
			ans+=1
			j+=1
		else:
			break;

	return ans;



###################################################################
# Function that calculates Consequtive Longest Common Subsequence
# starting from any character(CLCSn)
# ie. w1 = abcdef and w2 = abdef CLCSn = def
# Input: a and b are words 
# Output: Length of CLCSn
###############################################################
def clcsn(a,b):
	if len(a)>len(b):
		a,b = b,a

	max1=0;
	ans1=0;
	poss=0;

	for i in range(0,len(a)): 
		poss=i;

		for j in range(0,len(b)):

			if(a[poss]==b[j]):
				ans1+=1
				poss+=1
				if(poss>=len(a)):
					break

			else:
				if(ans1>0):
					max1=max(max1,ans1)
					ans1=0
					poss=i

		max1=max(max1,ans1)
		ans1=0
	return max1;


####################################################################
# Function that normalzes the score by the following formula-
# Normalized Score = Score^2 / (Length of word 1 * Length of word 2)
# Input: a is the score calculated by any of the above functions
#		 b and c are the words 
# Output: Normalized score out of 1
##################################################################
def norm(a,b,c):
	q=float(a*a);
	w1=float(len(b))
	w2=float(len(c))
	try:
		q=q/(w1*w2);
	except ZeroDivisionError:
		q=0

	return q;


##########################################################################
# Main Function which will be called for calculating Lexical Similarity-
# Input: a and b are two words 
# Output: Lexical Similarity score out of 1 (1 corresponds to exact match)
##########################################################################
def SyntacticSimilarity(a,b):	
	wght1=0.2;		#weight corresponding to LCS
	wght2=0.2;		#weight corresponding to CLCS1
	wght3=0.6;		#weight corresponding to CLCSn
	sum=norm(lcs(a,b,len(a),len(b)),a,b)*wght1+norm(clcs1(a,b),a,b)*wght2+norm(clcsn(a,b),a,b)*wght3;
	return sum


if __name__ == "__main__":
	print SyntacticSimilarity("what","what")