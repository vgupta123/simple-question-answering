################################################################		
# Code for calculating sentence scores for questions 
# Author : Maitrey Mehta
################################################################
from syntacticsimilarity import SyntacticSimilarity
import numpy as np
import nltk
from nltk.corpus import stopwords
from infoExtraction import questionTyping, info_extract
from nltk.stem import PorterStemmer

score_dict = {"word_match": 1, "strike_score":2}
default_dict= {"word_match": 0, "strike_score":0,"final_score":0,"pos_score":0,"word": ""}
stopwordss = set(stopwords.words('english'))
pos_score = {"CC": 1, "CD":2,"DT":0,"EX":0,"FW":5,"IN":1,"JJ":4,"JJR":4,"JJS":4,\
			"LS":0,"MD":0,"NN":5,"NNS":5,"NNP":6,"NNPS":6,"PDT":3,"POS":3,"PRP":2,\
			"PRP$":2,"RB":3,"RBR":3,"RBS":3,"RP":4,"TO":2,"UH":3,"VB":6,"VBD":6,"VBG":6,\
			"VBN":6,"VBP":6,"VBZ":6,"WDT":3,"WP":3,"WP$":3,"WRB":3}
ps=PorterStemmer()

word_imp={}

def getFreqdist(sentences):
	global word_imp
	dist = {}
	for i in sentences:
		i = i.replace('"','')
		sent_words = i.split(" ")
		sent_words = [w for w in sent_words if not w in stopwordss]
		sent_words = [i for i in sent_words if i!=""]
		for j in xrange(0,len(sent_words)):
			sent_words[j] = ps.stem(sent_words[j])
			if sent_words[j][-1] == ",":
				sent_words[j] = sent_words[j][:-1]
			if sent_words[j] not in dist.keys():
				dist[sent_words[j]] = 1.0
			else:
				dist[sent_words[j]] += 1.0
	
	word_imp = {k: (1/v) for k, v in dist.iteritems()}

#################################################################
# Function that takes question and a sentence and computes 
# word match and reqards sequential overlap in sentences
# Input : two strings question and sentence
# Output: List of dictionaries containing word match and 
#		  and sequence scores for word
#		  comb_Score returns the combined score for sentence
#		  with the question
################################################################
def calcScoreMatrix(ques,sent):
	global score_dict
	ques = ques.replace('"','')
	sent = sent.replace('"','')

	ques_words = ques.split(" ")
	sent_words = sent.split(" ")

	ques_words = [w for w in ques_words if not w in stopwordss]
	ques_words = [q for q in ques_words if q != '']
	sent_words = [w for w in sent_words if not w in stopwordss]
	sent_words = [i for i in sent_words if i!=""]
	pos_tags = nltk.pos_tag(sent_words)

	##########Stemming###########################
	for i in xrange(0,len(ques_words)):
		ques_words[i] = ps.stem(ques_words[i])
		if ques_words[i][-1] == ",":
				ques_words[i] = ques_words[i][:-1]
	for i in xrange(0,len(sent_words)):
		sent_words[i] = ps.stem(sent_words[i])
		if sent_words[i][-1] == ",":
				sent_words[i] = sent_words[i][:-1]
	#############################################
	
	matchMatrix = np.zeros((len(sent_words),len(ques_words)))

	for i in xrange(0,len(sent_words)):
		for j in xrange(0,len(ques_words)):
			if (len(ques_words[j])>20 or len(sent_words[i])>20) and (ques_words[j].lower() == sent_words[i].lower()):
				matchMatrix[i][j] = 0.0
				continue
			matchMatrix[i][j] = SyntacticSimilarity(ques_words[j].lower(),sent_words[i].lower())
			
	sent_scores = []
	for j in xrange(0,len(ques_words)):
		temp_dict = default_dict.copy()
		temp_dict["word"] = ques_words[j]
		for i in xrange(0,len(sent_words)):
			if matchMatrix[i][j]==1.0:
				temp_dict["word_match"] = score_dict["word_match"]
				try:
					temp_dict["pos_score"] = pos_score[pos_tags[i][1]]
				except:
					temp_dict["pos_score"] = 0
				if j != 0 and matchMatrix[i-1][j-1]==1.0:
					temp_dict["strike_score"] = score_dict["strike_score"]
				temp_dict["final_score"] = (temp_dict["word_match"] + temp_dict["strike_score"] + temp_dict["pos_score"])*word_imp[sent_words[i]]				######Change when changing score_dict
				break
		sent_scores.append(temp_dict)

	comb_score = 0
	for i in sent_scores:
		comb_score += i["final_score"]

	return sent_scores,comb_score


#################################################################
# Function that computes sentence similarity score of questions
# with sentences in text 
# Input : text in story(string), Array of question strings
################################################################
def sentScore(text,questions,id):
	sentences = text.split(" . ")
	getFreqdist(sentences)
	
	#f = open("./answers/"+str(id),"a+")
	for ques in questions:
		best_answer_score = -1
		ranked = []
		best_result= []
		best_sent = ""
		for sent in sentences:
			if sent == " " or sent=="":
				continue
			try:
				sent_word_scores, comb_score = calcScoreMatrix(ques["question"][:-1],sent)
			except:
			 	continue
			ranked.append({"sent":sent,"score":comb_score})
			if comb_score > best_answer_score:
				best_answer_score = comb_score
				best_sent = sent
				best_sent_scores = sent_word_scores
				#best_result = results

		# print "********************************************"
		# print "Question: " + str(ques["question"])
		# print "Difficulty: " + str(ques["diff"])
		# print "Best Sentence: " + str(best_sent) + "\n"
		#print "Answer: " + str(best_result)
		#print "Best Sent Scores: \n" + str(best_sent_scores)
		newlist = sorted(ranked, key=lambda k: k['score'],reverse=True)
		# for i in newlist[:5]:
		# 	print i["sent"]
		# 	print i["score"]
		question_dic = questionTyping(ques["question"])
		results,whole_sent = info_extract(question_dic,best_sent)
		results = " ".join(list(results))
		print "QuestionID: "+ques["qid"]
		if whole_sent==True and question_dic["type"] not in ["What","Why","How"]: 
			for i in newlist[1:3]:
				results2, whole_sent = info_extract(question_dic,i["sent"])
				if whole_sent==False:
					results = " ".join(list(results2))
					break
				  
		# #print len(newlist)
		print "Answer: " + str(results)+"\n"

		#print "*********************************************"
		#print "\n\n"

if __name__ =="__main__":
	ques = "Where has marijuana already been approved for medical use"
	sent = "The study would examine how the drug should be administered, and how a safe supply could be distributed to patients"
	calcScoreMatrix(ques,sent)