import sys
import nltk
from nltk.chunk import conlltags2tree, tree2conlltags
from nltk import word_tokenize, pos_tag, ne_chunk
import spacy
from spacy import displacy
from collections import Counter
import en_core_web_sm
nlp = en_core_web_sm.load()

##################################################################
# Function that does NER tagging, Question Typing for each sentence in question text
# Function finds correct answer substring from questions
# Input: text (string)
# Output: set of possible answers
# Function Author: Vivek Gupta
###################################################################

## function remove unwanted tags ##
# Function that does NER tagging for each sentence in reading text
# Input: text (string)
# Output: List of list. Inner list contains tuples of ('word',NER tag)
# Function Author: Vivek Gupta
###################################################################

# def get_continous_tags(tagged_sent):
#     continuous_tags = []
#     current_tag = []
#     # for token, pos, tag in tagged_sent:
#     for token, tag in tagged_sent:
#         if tag != "O":
#             current_tag.append((token, tag))
#         else:
#             if current_tag:
#                 continuous_tags.append(current_tag)
#                 current_tag = []
#     if current_tag:
#         continuous_tags.append(current_tag)

#     final_continuous_tag = [(" ".join([token for token, tag in ne]), ne[0][1]) for ne in continuous_tags]
#     return final_continuous_tag

## question typing functions ##
def questionTyping(question):
	iob_tagged = nlp(unicode(question))
	question_dic = {"question":question, "ner_tagged": [(X.text, X.label_) for X in iob_tagged.ents]}

	all_question_words = question.split()
	first_word_question = all_question_words[0]

	if "Who" in question:
		question_dic["type"] = "Who"
	elif "Where" in question:
		question_dic["type"] = "Where"
	elif "When" in question:
		question_dic["type"] = "When"
	elif "How" in question:
		question_dic["type"] = "How"
	elif "Which" in question:
		question_dic["type"] = "Which"
	elif "What" in question:
		question_dic["type"] = "What"
	elif "Why" in question:
		question_dic["type"] = "Why"
	elif "who" in question:
		question_dic["type"] = "Who"
	elif "where" in question:
		question_dic["type"] = "Where"
	elif "when" in question:
		question_dic["type"] = "When"
	elif "how" in question:
		question_dic["type"] = "How"
	elif "which" in question:
		question_dic["type"] = "Which"
	elif "what" in question:
		question_dic["type"] = "What"
	elif "why" in question:
		question_dic["type"] = "Why"
	elif "Does" in first_word_question:
		question_dic["type"] = "Does"
	elif "Is" in first_word_question:
		question_dic["type"] = "Is"
	elif "Was" in first_word_question:
		question_dic["type"] = "Was"

	#print "question_ner", question_dic["ner_tagged"]
	return question_dic

## infomation extraction function ##
def info_extract(question_dic,sentence):

	type_ques = question_dic["type"]

	iob_tagged = nlp(unicode(sentence))
	ner_tagged = [(X.text, X.label_) for X in iob_tagged.ents]
	#print "sentence_ner", ner_tagged


	question_nertags = question_dic['ner_tagged']
	
	sentence_tokens = nltk.word_tokenize(sentence)
	sentence_pos_tagged = nltk.pos_tag(sentence_tokens)

	question_tokens = nltk.word_tokenize(question_dic["question"])
	question_pos_tagged = nltk.pos_tag(question_tokens)
	
	possible_answers = []
	
	if type_ques == "Who":
		for item in ner_tagged:
			tag = item[1]
			if tag == "PERSON" or tag == "ORG" or tag == "NORP" or tag == "GPE":
				possible_answers += [item[0]]
	
	if type_ques == "Where":
		for item in ner_tagged:
			tag = item[1]
			if tag == "LOC" or tag == "FAC" or tag == "GPE" or tag == "PRODUCT" or tag == "WORK_OF_ART" or tag == "LAW":
				possible_answers += [item[0]]
	
	if type_ques == "When":
		for item in ner_tagged:
			tag = item[1]
			if tag == "DATE" or tag =="TIME":
				possible_answers += [item[0]]
	
	if type_ques == "How":
		if "much" in question_dic["question"]:
			for item in ner_tagged:
				tag = item[1]
				if tag == 'MONEY' or tag == 'QUANTITY' or tag == "CARDINAL" or tag == "PERCENT":
					possible_answers += [item[0]]
			if "time" in question_dic["question"]:
				tag = item[1]
				if tag == "TIME":
					possible_answers += [item[0]]				
		elif "many" in question_dic["question"]:
			for item in ner_tagged:
				tag = item[1]
				if tag == "CARDINAL" or tag == 'QUANTITY' or tag == "PERCENT":
					possible_answers += [item[0]]
		elif "did" in question_dic["question"].split(" ")[1]:
			possible_answers = []
		else:
			for item in ner_tagged:
				possible_answers += [item[0]]
	
	if type_ques == "Which":
		if "city" in question_dic["question"] or "country" in question_dic["question"]:
			for item in ner_tagged:
				tag = item[1]
				if tag == "GPE":
					possible_answers += [item[0]]
		elif "company" in question_dic["question"]:
			for item in ner_tagged:
				tag = item[1]
				if tag == "ORG":
					possible_answers += [item[0]]		
		else:
			for item in ner_tagged:
				tag = item[1]
				if tag == "ORG" or tag == "NORP" or tag == "GPE" or tag == "LOC":
					possible_answers += [item[0]]

	if type_ques == "What":
		if "organization" in question_dic["question"].split(" ")[1]:
			for item in ner_tagged:
				tag = item[1]
				if tag == "ORG" or tag == "NORP":
					possible_answers += [item[0]]
		else:
			tagged_words = []
			for item in ner_tagged:
				tagged_words += [item[0]]
			for item in sentence_pos_tagged:
				if "VB" in item[1] or "NN" in item[1]:
					if item[0] in tagged_words:
						possible_answers += [item[0]]
	
	if type_ques == "Why":
		flag_verbs = []
		for item in question_pos_tagged:
			if "VB" in item[1]:
				if item[0] in sentence:
					flag_verbs += [item[0]]
		for flag_word in flag_verbs:
			main_verb_pass = 0
			for item in sentence.split(" "):
				if main_verb_pass == 1:
					possible_answers += [item]
				if main_verb_pass == 0:
					if flag_word == item:
						main_verb_pass = 1

	if type_ques == "Does" or type_ques == "Is" or type_ques == "Was":
		negation = ["no", "without", "nil","not", "n't", "never", "none", "neith", "nor", "non", "deny", "reject", "refuse", "subside", "retract", "non"]

		matched = []
		unmatched = []
		for itemQues in question_nertags:
			for itemSen in ner_tagged:
				if itemQues[1] == itemSen[1]:
					if itemQues[0] == itemSen[0]:
						matched += [itemSen[0]]
					else:
						unmatched += [(itemQues[0],itemSen[0])]
		neg_flag = 0
		for neg in negation:
			if neg in sentence:
				neg_flag += 1
		if len(unmatched) > 0:
			if neg_flag == 0:
				possible_answers = ["No"]
			else:
				possible_answers = ["Yes"]
		possible_answers = ["Yes"]
	
	for item in question_nertags:
		if item[0] in possible_answers:
			possible_answers.remove(item[0])
	final_answers = set(possible_answers)
	whole_sent=False
	if final_answers == set():
		final_answers = set([sentence])
		whole_sent=True

	return final_answers,whole_sent


if __name__=="__main__":
	data = open("gold_qas.txt").read()
	data = data.split("\n")

	for datapoint in xrange(0,len(data),4):
		question = data[datapoint]
		correct_ans = data[datapoint+1]
		sentence = data[datapoint+2]
		print "question:", question
		print "sentence:", sentence
		print "correct_answer:", correct_ans
		print "--------------------------"
		question_dic = questionTyping(question)
		results = info_extract(question_dic,sentence)
		print "our_answer:", results
		print "--------------------------"

