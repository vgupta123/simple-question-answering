Question: How much does Alexi Yashin earn as a hockey player?
Best Sentence: The government has been looking at a charitable donation by Ottawa Senators hockey star Alexi Yashin
Best Sent Scores: 
[{'word_match': 0, 'final_score': 0, 'word': 'How', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'much', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'does', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'Alexi', 'strike_score': 0}, {'word_match': 1, 'final_score': 3, 'word': 'Yashin', 'strike_score': 2}, {'word_match': 0, 'final_score': 0, 'word': 'earn', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'as', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'a', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'hockey', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'player?', 'strike_score': 0}]



Question: Who does Alexi Yashin play for?
Best Sentence: The government has been looking at a charitable donation by Ottawa Senators hockey star Alexi Yashin
Best Sent Scores: 
[{'word_match': 0, 'final_score': 0, 'word': 'Who', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'does', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'Alexi', 'strike_score': 0}, {'word_match': 1, 'final_score': 3, 'word': 'Yashin', 'strike_score': 2}, {'word_match': 0, 'final_score': 0, 'word': 'play', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'for?', 'strike_score': 0}]



Question: How much money did Alexi Yashin actually donate to the National Arts Centre?
Best Sentence:  Earlier this year he promised to give one million dollars to the National Arts Centre, a concert hall where people go to see plays and dance and to hear live music
Best Sent Scores: 
[{'word_match': 0, 'final_score': 0, 'word': 'How', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'much', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'money', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'did', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'Alexi', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'Yashin', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'actually', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'donate', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'to', 'strike_score': 0}, {'word_match': 1, 'final_score': 3, 'word': 'the', 'strike_score': 2}, {'word_match': 1, 'final_score': 3, 'word': 'National', 'strike_score': 2}, {'word_match': 1, 'final_score': 3, 'word': 'Arts', 'strike_score': 2}, {'word_match': 0, 'final_score': 0, 'word': 'Centre?', 'strike_score': 0}]



Question: Why do people go to the National Arts Centre?
Best Sentence:  Earlier this year he promised to give one million dollars to the National Arts Centre, a concert hall where people go to see plays and dance and to hear live music
Best Sent Scores: 
[{'word_match': 0, 'final_score': 0, 'word': 'Why', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'do', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'people', 'strike_score': 0}, {'word_match': 1, 'final_score': 3, 'word': 'go', 'strike_score': 2}, {'word_match': 1, 'final_score': 1, 'word': 'to', 'strike_score': 0}, {'word_match': 1, 'final_score': 3, 'word': 'the', 'strike_score': 2}, {'word_match': 1, 'final_score': 3, 'word': 'National', 'strike_score': 2}, {'word_match': 1, 'final_score': 3, 'word': 'Arts', 'strike_score': 2}, {'word_match': 0, 'final_score': 0, 'word': 'Centre?', 'strike_score': 0}]



Question: How would Alexi's parents have benefited from his donation to the National Arts Centre?
Best Sentence:  Earlier this year he promised to give one million dollars to the National Arts Centre, a concert hall where people go to see plays and dance and to hear live music
Best Sent Scores: 
[{'word_match': 0, 'final_score': 0, 'word': 'How', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'would', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': "Alexi's", 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'parents', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'have', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'benefited', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'from', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'his', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'donation', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'to', 'strike_score': 0}, {'word_match': 1, 'final_score': 3, 'word': 'the', 'strike_score': 2}, {'word_match': 1, 'final_score': 3, 'word': 'National', 'strike_score': 2}, {'word_match': 1, 'final_score': 3, 'word': 'Arts', 'strike_score': 2}, {'word_match': 0, 'final_score': 0, 'word': 'Centre?', 'strike_score': 0}]



Question: Where did reporters question Alexi Yashin?
Best Sentence:  After the game reporters went to the locker room to ask Alexi Yashin what was going on
Best Sent Scores: 
[{'word_match': 0, 'final_score': 0, 'word': 'Where', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'did', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'reporters', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'question', 'strike_score': 0}, {'word_match': 1, 'final_score': 1, 'word': 'Alexi', 'strike_score': 0}, {'word_match': 0, 'final_score': 0, 'word': 'Yashin?', 'strike_score': 0}]



